import React from 'react';

import Logo from './logo';
import Menu from './menu';

import './styles.css';

const Header = ({selected}) => {

      return (
            <header>
                  <Logo ></Logo>
                  <Menu selected={selected}></Menu>
            </header>
      );
};

export default Header;
