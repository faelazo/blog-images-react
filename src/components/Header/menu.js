import React from 'react';

import {MENU} from './../constants';

const getOptions = selected =>{
      return MENU.map((option)=>{
            return (<li key={option} className="menuOption" onClick={() => {selected(option)}}>{option.toUpperCase()}</li>);
      })
};

//<li onClick={() => seleccion('home')} value="home">INICIO</li>
const Menu = ({selected}) => {
      return (
            <div id="divMenu">
                  <nav id="menu">
                        <ul>
                              {getOptions(selected)}
                        </ul>
                  </nav>
            </div>
      );
}

export default Menu;
