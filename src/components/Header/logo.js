import React from 'react';
import logo from './../../img/logo.png';

const Logo = () => {

      return (
            <div id="divLogo">
                  <img id="logo" src={logo} alt="RecipeBook"/>
                  <h1 id="nameLogo">IMAGES BLOG</h1>
            </div>
      );
};

export default Logo;
