import React, { Component } from 'react';

import Home from './home';
import Blog from './blog';
import Images from './images';
import Statistics from './statistics';
import About from './about';

import './styles.css';
import data_images from './../../data/images.json';
import data_posts from './../../data/posts.json';

class Content extends Component{

      getSelectedOption = () => {
            switch (this.props.selectedOption) {
                  case 'blog':
                        return (<Blog data_posts={data_posts} handleShowPost={this.props.handleShowPost}></Blog>);
                  case 'images':
                        return (<Images handleShowImage={this.props.handleShowImage} data_images={data_images}></Images>);
                  case 'statistics':
                        return (<Statistics data_images={data_images} data_posts={data_posts}></Statistics>);
                  case 'about':
                        return (<About></About>);
                  default:
                        return (<Home data_images={data_images} data_posts={data_posts} handleShowImage={this.props.handleShowImage} handleShowPost={this.props.handleShowPost}></Home>);
            }
      };

      render(){

            return (
                  <article id="divContent">
                        {this.getSelectedOption()}
                  </article>
            );
      };
};

export default Content;
