import React from 'react';

import ListImagePost from './posts/list-image-post';

import './styles.css';

const Images = ({handleShowImage, data_images}) => {

      const listImages = () => {
            return data_images.map(image => {
                  return (
                        <ListImagePost key={image.id} handleShowImage={handleShowImage} image={image}></ListImagePost>
                  );
            });
      };

      return (
            <div className="divContentMain" id="divImages">
                  {listImages()}
            </div>
      );
};

export default Images;
