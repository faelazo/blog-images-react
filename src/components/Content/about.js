import React from 'react';

import './styles.css';

const About = () => {
      return (
            <div className="divContentMain" id="divAbout">
                  <h1>About us</h1>
                  <p>¿Acerca de mí? Me presentaré como un aficionado a la fotografía. Llevo con mi reflex desde mayo de 2017 y desde entonces he encontrado en ella una mirilla desde la que observo el mundo de una manera especial. Empecé a salir a fotografiar todo aquello que me resultaba peculiar y terminé encontrando peculiaridad en cada rincón que visito. El día que dije: ¡ojalá tuviese aquí mi cámara! me di cuenta de que este mundillo me apasiona.</p>
                  <p>Después de unos meses acumulando fotografías, las cuales enseñaba a mis seres más queridos y cercanos, pensé que podía buscar un sitio donde mostrarlas al resto del mundo que quisiera verlas. Esta decisión fue lo que me llevó a crear esta web donde muestro algunas de mis interpretaciones.</p>
                  <p>Espero que todos los que visitéis este rinconcito de Internet lo consideréis un espacio bien aprovechado.</p>
                  <p>Y como nunca está de más, se pueden adquirir algunos de mis trabajos en la red de ShutterStock. Accederéis a mi perfil clicando aquí, aunque yo, con la más absoluta sinceridad, ya me siento satisfecho si he conseguido haceros disfrutar durante el ratito que habéis permanecido en mi rincón.</p>
                  <p>Saludos y hasta pronto ;).</p>  
            </div>
      );
};

export default About;
