import React from 'react';
import { VictoryPie } from 'victory';

import './styles.css';

const StatisticsPie = ({data_images, data_posts}) => {

      const colors = ["tomato", "orange", "gold", "cyan", "navy", "CornflowerBlue", "Crimson", "DarkGreen", "DarkKhaki", "DarkOrange", "Gold", "GreenYellow", "HotPink", "steelBlue" ];

      const getDataImages = () => {
            return data_images.map(item => {
                  var data = {
                        x: item.title.toUpperCase() + " (" + item.likes + ")",
                        y: item.likes
                  };
                  return data;
            });
      };

      const getDataPosts = () => {
            return data_posts.map(item => {
                  var data = {
                        x: item.title.toUpperCase(),
                        y: item.likes
                  };
                  return data;
            });
      };

      return (
            <div className="divContentMain" id="divStatistics">
                  <h1>Graphic Images</h1>
                  <VictoryPie id="pieImages" data={getDataImages()} colorScale={colors}/>
                  <h1>Graphic Posts</h1>
                  <VictoryPie id="piePosts" data={getDataPosts()} colorScale={colors}/>
            </div>
      );
};

export default StatisticsPie;
