import React from 'react';

import './styles.css';

const imagePost = ({handleExitImage, handleClickStar, image}) => {

      return (
            <div className="imagePostClass">
                  <div className="divImage">
                        <img className="imgMaxPost" src={image.pageURL} alt="Málaga" ></img>
                  </div>
                  <div className="divExit">
                        <h1 onClick={() => handleExitImage()}>X</h1>
                  </div>
                  <div className="divExif">
                        <h3>{image.title.toUpperCase()}</h3>
                        <button className={'btStar'+image.like} onClick={() => handleClickStar(image)}></button>
                        <ul>
                              <li><p>Speed = {image.speed}</p></li>
                              <li><p>Aperture = {image.aperture}</p></li>
                              <li><p>ISO = {image.iso}</p></li>
                        </ul>
                        <h3>Description</h3>
                        <p>{image.description}</p>
                  </div>
            </div>
      );
};

export default imagePost;
