import React from 'react';

import './styles-min.css';

const PostMin = ({post, handleShowPost}) => {
      return (
            <div className="postMinClass" onClick={() => {handleShowPost(post)}}>
                  <h1 className="h1PostMin">{post.title}</h1>
                  <p className="pDatePubli">Published at {post.datePublic}</p>
                  <section className="sectionPostMin">
                        <p className="pPostMin">{post.content}</p>
                  </section>
            </div>
      );
};

export default PostMin;
