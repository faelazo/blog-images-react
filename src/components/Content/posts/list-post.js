import React from 'react';

import './styles-lists.css';

const ListPost = ({post, handleShowPost}) => {
      return (
            <div className="listPostClass" onClick={() => {handleShowPost(post)}}>
                  <h1 className="h1Post">{post.title}</h1>
                  <p className="pDatePubli">Published at {post.datePublic}</p>
                  <section className="sectionPost">
                        <p>{post.content}</p>
                  </section>
                  <h3 className="h3Post">Read more</h3>
            </div>
      );
};

export default ListPost;
