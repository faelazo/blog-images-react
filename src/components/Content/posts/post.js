import React from 'react';

import './styles.css';

const post = ({handleExitPost, handleClickStar, post}) => {
      return (
            <div className="postClass">
                  <div className="divExit">
                        <h1 onClick={() => handleExitPost()}>X</h1>
                  </div>
                  <div className="divExif">
                        <h3>{post.title}</h3>
                        <button className={'btStar'+post.like} onClick={() => handleClickStar(post)}></button>
                        <h3>Description</h3>
                        <p>{post.content}</p>
                  </div>
            </div>
      );
};

export default post;
