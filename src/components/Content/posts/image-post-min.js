import React from 'react';

import './styles-min.css';

const ImagePostMin = ({image, handleShowImage}) => {
      return (
            <div className="imagePostMinClass" onClick={() => {handleShowImage(image)}}>
                  <h1 className="h1PostMin">{image.title.toUpperCase()}</h1>
                  <p className="pDatePubli">Published at {image.datePublic}</p>
                  <img className="imgPostMin" src={image.pageURL} alt="Málaga"></img>
                  <section className="sectionPostMin">
                        <p className="pPostMin">{image.description}</p>
                  </section>
            </div>
      );
};

export default ImagePostMin;
