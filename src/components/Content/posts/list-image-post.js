import React from 'react';

import './styles-lists.css';

const ListImagePost = ({handleShowImage, image}) => {

      return (
            <div className="listImagePostClass" onClick={() => {handleShowImage(image)}}>
                  <h1 className="h1Post">IMAGE HEADER</h1>
                  <p className="pDatePubli">Published at {image.datePublic}</p>
                  <img className="imgPost" src={image.pageURL} alt="Málaga" ></img>
                  <section className="sectionPost">
                        <p className="pImagePost">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut feugiat mi. Cras suscipit nulla tortor, id aliquam enim dictum vitae. Sed odio odio, maximus at scelerisque sed, accumsan ut sapien. Aenean eu ultricies neque, ut fermentum leo. Nunc quam urna, luctus sit amet purus non, tempus dignissim sem. Mauris consectetur orci ut varius venenatis. Nunc a mattis urna, at dignissim arcu. Vivamus hendrerit ligula eros, nec tincidunt dui volutpat sit amet. Vivamus commodo eget diam eget vestibulum.</p>
                  </section>
            </div>
      );
};

export default ListImagePost;
