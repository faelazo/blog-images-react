import React from 'react';

import PostMin from './posts/post-min';
import ImagePostMin from './posts/image-post-min';

import './styles.css';

const Home = ({data_images, data_posts, handleShowImage, handleShowPost}) => {

      const getPublic = () => {
            var data = data_images.concat(data_posts);

            return data.sort((a, b)=> {
                  return Date.parse(a.datePublic) - Date.parse(b.datePublic)
            }).map(item => {
                  if (item.type === "IMAGE"){
                        return (<ImagePostMin image={item} handleShowImage={handleShowImage}></ImagePostMin>);
                  }else{
                        return (<PostMin post={item} handleShowPost={handleShowPost}></PostMin>);
                  }
            });
      };

      return (
            <div className="divContentMain" id="divHome">
                  {getPublic()}
            </div>
      );
};

export default Home;
