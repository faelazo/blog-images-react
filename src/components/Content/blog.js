import React from 'react';

import ListPost from './posts/list-post';

import './styles.css';

const Blog = ({data_posts, handleShowPost}) => {

      const listPosts = () => {
            return data_posts.map(post => {
                  return (
                        <ListPost key={post.id} post={post} handleShowPost={handleShowPost}></ListPost>
                  );
            });
      };

      return (
            <div className="divContentMain" id="divBlog">
                  {listPosts()}
            </div>
      );
};

export default Blog;
