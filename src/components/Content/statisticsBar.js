import React from 'react';
import { VictoryChart } from 'victory';
import { VictoryAxis } from 'victory';
import { VictoryBar } from 'victory';

import './styles.css';

const StatisticsBar = ({data_images, data_posts}) => {

      const getDataImagesBar = () => {
            return data_images.map(item => {
                  var data = {
                        x: item.title.toUpperCase(),
                        y: item.likes
                  };
                  return data;
            });
      };

      const getDataPostsBar = () => {
            return data_posts.map(item => {
                  var data = {
                        x: item.title.toUpperCase(),
                        y: item.likes
                  };
                  return data;
            });
      };

      return (
            <div className="divContentMain" id="divStatistics">
                  <h1>Graphic Images</h1>
                  <VictoryChart >
                        <VictoryAxis      independentAxis
                                          style={{    tickLabels: { fill: "black", angle: 90, fontSize: 10, padding: 25 }}}/>
                        <VictoryBar id="barImages"
                                    data={getDataImagesBar()} 
                                    events={[{
                                          target: "data",
                                          eventHandlers: {
                                            onMouseOver: () => {
                                              return [
                                                {
                                                  target: "data",
                                                  mutation: (props) => {
                                                    return { style: { fill: "yellow" } };
                                                  }
                                                }
                                              ];
                                            },
                                            onMouseOut: () => {
                                                return [
                                                  {
                                                    target: "data",
                                                    mutation: (props) => {
                                                      return { style: { fill: "CornflowerBlue" } };
                                                    }
                                                  }
                                                ];
                                              }
                                          }
                                        }]}
                                    labels={(d) => d.y}
                                    style={{    data: {fill: "CornflowerBlue" }}}/>
                  </VictoryChart>
                  <h1>Graphic Posts</h1>
                  <VictoryChart >
                        <VictoryAxis      independentAxis
                                          style={{    tickLabels: { fill: "black", angle: 90, fontSize: 10, padding: 25 }}}/>
                        <VictoryBar id="barPosts"
                                    data={getDataPostsBar()} 
                                    events={[{
                                          target: "data",
                                          eventHandlers: {
                                            onMouseOver: () => {
                                              return [
                                                {
                                                  target: "data",
                                                  mutation: (props) => {
                                                    return { style: { fill: "yellow" } };
                                                  }
                                                }
                                              ];
                                            },
                                            onMouseOut: () => {
                                                return [
                                                  {
                                                    target: "data",
                                                    mutation: (props) => {
                                                      return { style: { fill: "CornflowerBlue" } };
                                                    }
                                                  }
                                                ];
                                              }
                                          }
                                        }]}
                                    labels={(d) => d.y}
                                    style={{    data: {fill: "CornflowerBlue" }}}/>
                  </VictoryChart>
            </div>
      );
};

export default StatisticsBar;
