import React, { Component } from 'react';

import './styles.css';
import StatisticsPie from './statisticsPie';
import StatisticsBar from './statisticsBar';

class Statistics extends Component {

      constructor(){
            super();
            this.state = {
                  showGraphic: "PIE"
            };
      };

      handleRadios = (seleccion) => {this.setState({showGraphic: seleccion})}

      getSelection = () => {
            switch (this.state.showGraphic) {
                   case 'BAR':
                        return (<StatisticsBar id="idStatisticsBar" data_images={this.props.data_images} data_posts={this.props.data_posts}/>);
                  default:
                        return (<StatisticsPie id="idStatisticsPie" data_images={this.props.data_images} data_posts={this.props.data_posts}/>);
            }
      }

      render(){
            return (
                  <div className="divContentMain" id="divStatistics">
                        <div id="selector"  >
                              <h3>Seleccionar:</h3>
                              <input type="radio" value="pie" name="selectGraphic" onChange={() => this.handleRadios("PIE")} defaultChecked />
                              Tarta
                              <input type="radio" value="bar" name="selectGraphic" onChange={() => this.handleRadios("BAR")}/>
                              Barras
                        </div>
                        {this.getSelection()}
                  </div>
            );
      }
};

export default Statistics;
