import React, { Component } from 'react';
import Header from './components/Header';
import Content from './components/Content';
import ImagePost from './components/Content/posts/image-post';
import Post from './components/Content/posts/post';
import './App.css';

class App extends Component {

      constructor(){
            super();
            this.state = {
                  showMyImage: false,
                  showMyPost: false,
                  menuOptionSelected: "home"
            };
      };

      handleExitImage = () => {
            this.setState({showMyImage: false});
      };

      handleShowImage = image => {
            this.setState({
                  showMyImage: true,
                  imageLoad: image
            });
      };

      handleShowPost = post => {
            this.setState({
                  showMyPost: true,
                  postLoad: post
            });
      };

      handleExitPost = () => {
            this.setState({showMyPost: false});
      };

      handleClickStarImage = (star) => {
            //Get image loaded
            var starChanged = this.state.imageLoad;

            //Change star of image loaded
            starChanged.like = Math.abs(star.like-1)

            //Set image changed with new star value
            this.setState({imageLoad: starChanged});
      };

      handleClickStarPost = (star) => {
            //Get image loaded
            var starChanged = this.state.postLoad;

            //Change star of image loaded
            starChanged.like = Math.abs(star.like-1)

            //Set image changed with new star value
            this.setState({postLoad: starChanged});
      };

      selectedOption = option => {
            this.setState({menuOptionSelected: option});
      };

      render() {
            return (
                  <div className="App">
                        { this.state.showMyImage ? <ImagePost handleExitImage={this.handleExitImage} handleClickStar={this.handleClickStarImage} image={this.state.imageLoad} /> : null }
                        { this.state.showMyPost ? <Post handleExitPost={this.handleExitPost} handleClickStar={this.handleClickStarPost} post={this.state.postLoad} /> : null }
                        <Header selected={this.selectedOption}></Header>
                        <Content handleShowImage={this.handleShowImage} handleShowPost={this.handleShowPost} selectedOption={this.state.menuOptionSelected}></Content>
                  </div>
            );
      }
}

export default App;
